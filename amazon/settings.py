# -*- coding: utf-8 -*-

# Scrapy settings for amazon project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'amazon'

SPIDER_MODULES = ['amazon.spiders']
NEWSPIDER_MODULE = 'amazon.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'amazon (+http://www.yourdomain.com)'

USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36"
DOWNLOAD_DELAY = 0.5
RANDOMIZE_DOWNLOAD_DELAY = True

DOWNLOADER_MIDDLEWARES = {
	    'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': 400,
	    'scrapy.contrib.downloadermiddleware.cookies.CookiesMiddleware': 700,
}

COOKIES_ENABLED = True
COOKIES_DEBUG  	= True

# message quueue pipeline settings
BROKER_HOST = '178.62.103.124'
BROKER_PORT = 5672
BROKER_USERID = 'scrapy_mq_user'
BROKER_PASSWORD = "scrapy12345$$"
BROKER_VIRTUAL_HOST = "/"
# end message que pipeline settings

ITEM_PIPELINES = {
	'amazon.pipelines.MessageQueuePipeline': 300,
}

