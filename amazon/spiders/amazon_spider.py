from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.lxmlhtml import LxmlLinkExtractor
from scrapy.selector import Selector
from amazon.items import Product, CategoryItem
from scrapy import log


class AmazonSpider(CrawlSpider):

    name = 'amazon'
    allowerd_domains = ['amazon.co.uk', 'amazon.com']
    start_urls = ["http://www.amazon.com/gp/site-directory/"]

    rules = (
    	# extract the categories in the homepage
    	Rule(LxmlLinkExtractor(allow=(r'.*/b')), callback="parse_category", follow=True),
    	# extract the subcategories in the categories page
    	#Rule(SgmlLinkExtractor(allow=()))
    	#Rule(SgmlLinkExtractor(allow=(r'dp/[a-z0-9]*')), callback="pars_old", follow=True),
    )

    
    def parse_category(self, response):

        sel = Selector(response)
        items = []

        categories = sel.css('.nav_cat_links')

        for category in categories:
            item = CategoryItem()
            item['title'] = category.xpath('a/text()').extract()
            item['url']   = category.xpath('a/@href')
            items.append(item)            
            #log.msg(item['tilte'], level=log.DEBUG)

        return items


    def parse_amazon_product(self, response):

    	xpathSelector = Selector(response)
    	items = []

    	item = Product()
    	item['title'] 	= xpathSelector.xpath('//*[@id="productTitle"]')
    	item['link'] 	= response.url
    	item['desc'] 	= xpathSelector.xpath('//*[@id="feature-bullets"]/ul')
    	item['price']	= xpathSelector.xpath('//*[@id="priceblock_ourprice"]')
    	item['inStock'] = xpathSelector.xpath('//*[@id="availability"]/span')
    	#item['isPrime'] = xpathSelector.xpath('//*[@id="availability"]/span')
    	item['longDesc'] = xpathSelector.xpath('//*[@id="productDescription"]/div/div')
    	items.append(item)	

    	return items

    
    
     