# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Product(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    link = scrapy.Field()
    desc = scrapy.Field()
    longDesc = scrapy.Field()
    price = scrapy.Field()
    inStock = scrapy.Field()
    isPrime = scrapy.Field()


class CategoryItem(scrapy.Item):

	title = scrapy.Field()
	url = scrapy.Field()
